<?php

const APP_CONFIG =  [
    'DB_HOST' => 'mysql',
    'DB_DATABASE' => 'test',
    'DB_USER' => 'test',
    'DB_PASSWORD' => '6316d1e1eb5a113d688d33c6449c2e48',
    'REDIS_HOST' => 'redis',
    'REDIS_PASSWORD' => '8eaee088b7dc977d0a90312ed55e3161',
];

function getConfig($name)
{
    if (array_key_exists($name, APP_CONFIG)) {
        return APP_CONFIG[$name];
    }
    throw new Exception("Variable $name doesn't exist");
}
