<?php

use App\Model\Counter;
use App\Services\Cache;
use App\Services\CounterFormatter;

require __DIR__.'/../vendor/autoload.php';
require __DIR__ . "/../config/config.php";

function collectData()
{
    $redisConnection = \App\DBAL\RedisConnection::getConnection();

    $countries = (new \App\Model\Country())->getAll();
    $countries = keyBy($countries, 'iso');
    $events = (new \App\Model\Event())->getAll();
    $events = keyBy($events, 'name');


    $keys = $redisConnection->keys('*');
    foreach ($keys as $key) {
        $data = explode('_', $key);
        if (!isset($data[0]) and !isset($data[1])) {
            continue;
        }
        $country = $data[0];
        $event = $data[1];
        if (array_key_exists($country, $countries) and array_key_exists($event, $events)) {
            $count = $redisConnection->get($key);
            (new \App\Model\Counter())->createOrUpdate($countries[$country]['id'], $events[$event]['id'], $count);
            $redisConnection->set($key, 0);
        }
        continue;
    }
}

function cacheAggregateData()
{
    $availableFormats = [
        'csv',
        'json',
    ];
    $connection = \App\DBAL\RedisConnection::getConnection();
    foreach ($availableFormats as $format) {
        $key = "data_$format";
        $data = (new Counter())->getTopAggregated();
        $method = 'format' . ucfirst($format);
        $formatted = CounterFormatter::$method($data);
        $connection->set($key, $formatted);
    }
}

function keyBy($array, $key)
{
    $res = [];
    foreach ($array as $value) {
        $res[$value[$key]] = $value;
    }
    return $res;
}

collectData();
cacheAggregateData();