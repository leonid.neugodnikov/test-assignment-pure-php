<?php

require __DIR__.'/../vendor/autoload.php';
require __DIR__ . "/../config/config.php";

function initializeDB()
{
    $connection = \App\DBAL\MysqlConnection::getConnection();
    $sql = "CREATE TABLE `countries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `iso` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
   PRIMARY KEY ( id )
) ENGINE=InnoDB;

CREATE TABLE `events` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
   PRIMARY KEY ( id )
) ENGINE=InnoDB;

create table counters (
   id INT NOT NULL AUTO_INCREMENT,
   country_id int unsigned NOT NULL,
   event_id int unsigned NOT NULL,
   count int unsigned NOT NULL,
   date date not null,
   PRIMARY KEY ( id ),
   FOREIGN KEY (country_id) REFERENCES countries(id),
   FOREIGN KEY (event_id) REFERENCES events(id)
) ENGINE = INNODB ;";

    $connection->exec($sql);
}

function populateCountries()
{
    $connection = \App\DBAL\MysqlConnection::getConnection();
    $connection->beginTransaction();

    $countries = file_get_contents(__DIR__."/../storage/SXGeo/country.tsv");
    $countries = explode("\n", $countries);
    $countries = array_filter($countries);
    foreach ($countries as $country) {
        $data = explode("\t", $country);

        $sql = "INSERT INTO countries (iso, name) VALUES (:iso, :name)";
        $connection->prepare($sql)->execute([
            'iso' => $data[1],
            'name' => $data[4],
        ]);
    }
    $connection->commit();
}

function populateEvents()
{
    $connection = \App\DBAL\MysqlConnection::getConnection();
    $events = [
        'view',
        'play',
        'click',
    ];
    $connection->beginTransaction();
    foreach ($events as $event) {
        $sql = "INSERT INTO events (name) VALUES (:name)";
        $connection->prepare($sql)->execute([
            'name' => $event,
        ]);
    }
    $connection->commit();
}

initializeDB();
populateCountries();
populateEvents();
