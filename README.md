In order to start project use "bootstrap.sh"
In order to populate DB with initial values, use the following command:

docker exec -ti php7 sh -c "php -f console/populateDB.php"
