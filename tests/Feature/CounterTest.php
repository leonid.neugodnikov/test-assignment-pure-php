<?php

namespace Tests\Feature;

use App\DBAL\MysqlConnection;
use App\Model\Counter;
use App\Model\Country;
use App\Model\Event;
use DateInterval;
use DatePeriod;
use DateTime;
use PHPUnit\Framework\TestCase;

class CounterTest extends TestCase
{

    /** @test */
    public function counter_aggregation_test()
    {
        $connection = MysqlConnection::getConnection();
        $connection->beginTransaction();

        $start = new DateTime('- 1 month');
        $end = new DateTime();
        $interval = new DateInterval('P1D');
        $dateRange = new DatePeriod($start, $interval, $end);

        $topCountries = [
            'US',
            'CA',
            'CH',
            'UA',
            'IT',
        ];
        $restCountries = [
            'NL',
            'AT',
            'FR',
            'BG',
        ];
        $events = [
            'click',
            'play',
            'view',
        ];

        $counterModel = new Counter();
        $countryModel = new Country();
        $eventModel = new Event();

        $checkArray = [];

        foreach ($dateRange as $date) {
            $inAggregate = $end->diff($date)->days <= 7 ? true : false;
            foreach ($topCountries as $countryIso) {
                $country = $countryModel->findOneByIso($countryIso);
                $countryId = $country[0]['id'];
                foreach ($events as $eventName) {
                    $event = $eventModel->findOneByName($eventName);
                    $eventId = $event[0]['id'];
                    $count = rand(1000, 5000);
                    if ($inAggregate) {
                        $checkArray[$countryIso][$eventName] = !isset($checkArray[$countryIso][$eventName]) ? $count : $checkArray[$countryIso][$eventName] + $count;
                    }
                    $counterModel->create($date->format('Y-m-d'), $countryId, $eventId, $count);
                }
            }
            foreach ($restCountries as $countryIso) {
                $country = $countryModel->findOneByIso($countryIso);
                $countryId = $country[0]['id'];
                foreach ($events as $eventName) {
                    $event = $eventModel->findOneByName($eventName);
                    $eventId = $event[0]['id'];
                    $count = rand(100, 1000);
                    $counterModel->create($date->format('Y-m-d'), $countryId, $eventId, $count);
                }
            }
        }
        $aggregatedData = $counterModel->getTopAggregated();
        foreach ($aggregatedData as $value) {
            $this->assertEquals($value['sum'], $checkArray[$value['country']][$value['event']]);
        }
        $connection->rollBack();
    }

}
