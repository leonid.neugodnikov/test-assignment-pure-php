#!/usr/bin/env bash

docker-compose down && docker-compose up -d --build && docker exec -ti php7 sh -c "./composer.phar install"
