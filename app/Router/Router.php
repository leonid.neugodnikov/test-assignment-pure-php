<?php

namespace App\Router;

use PHPUnit\Runner\Exception;

class Router
{
    protected $routes = [
        'get' => [
            '/get-info' => 'Controller@getInfo',
        ],
        'post' => [
            '/increase-counter' => 'Controller@increaseCounter'
        ],
    ];

    public function handle()
    {
        $verb = strtolower($_SERVER['REQUEST_METHOD']);
        $requestUri = $_SERVER['REQUEST_URI'];
        $requestUri = parse_url($requestUri);
        $path = $requestUri['path'];
        if (array_key_exists($verb, $this->routes) and array_key_exists($path, $this->routes[$verb])) {
            $handler = $this->routes[$verb][$path];
            $handler = explode('@', $handler);
            $class = $handler[0];
            $class = "\App\Controller\\$class";
            $method = $handler[1];
            $controller = new $class;
            $controller->$method($this->getRequestPayload($verb));
        } else {
            throw new \Exception('Route not found', 404);
        }
    }

    protected function getRequestPayload($verb)
    {
        switch ($verb) {
            case 'get':
                return $_GET;
            case 'post':
                if ($_SERVER['HTTP_CONTENT_TYPE'] == 'application/json') {
                    return json_decode(file_get_contents('php://input'), true);
                }
                return $_POST;
            default:
                throw new Exception('This method is not supported');
        }
    }

}