<?php

namespace App\Controller;

use App\DBAL\RedisConnection;
use App\Model\Counter;
use App\Services\Cache;
use App\Services\CounterFormatter;
use PHPUnit\Runner\Exception;

class Controller
{
    public function getInfo(array $request)
    {
        $availableFormats = [
            'csv',
            'json',
        ];
        $format = (isset($request['format']) and in_array($request['format'], $availableFormats)) ? $request['format'] : 'json';

        $data = Cache::remember("data_$format", function() use ($format) {
            $data = (new Counter())->getTopAggregated();
            $method = 'format' . ucfirst($format);
            $formatted = CounterFormatter::$method($data);
            return $formatted;
        });

        $contentType = $format == 'json' ? 'application/json' : 'text/csv';
        header("Content-Type: $contentType");

        echo $data;
    }

    public function increaseCounter(array $request)
    {
        if (!array_key_exists('country', $request) or !array_key_exists('event', $request)) {
            throw new Exception('Invalid payload for increment request');
        }

        $redisConnection = RedisConnection::getConnection();
        $redisConnection->incr("{$request['country']}_{$request['event']}");

        header('Content-Type: application/json');
        echo json_encode([
            'status' => 'ok',
        ]);
    }

}