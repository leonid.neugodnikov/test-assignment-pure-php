<?php

require __DIR__.'/../vendor/autoload.php';
require __DIR__."/../config/config.php";

$router = new \App\Router\Router();

try {
    $router->handle();
} catch (\Exception $e) {
    $code = $e->getCode() == 404 ? '404' : '500';
    http_response_code($code);
    readfile(__DIR__."/../resources/views/$code.html");
}
