<?php

namespace App\DBAL;


use Predis\Client;

class RedisConnection
{
    private static $connection = null;

    private function __construct()
    {
    }

    public static function getConnection()
    {
        if (self::$connection === null) {

            $host = getConfig('REDIS_HOST');
            $password = getConfig('REDIS_PASSWORD');
            $parameters = [
                'host' => $host,
            ];
            $options = [
                'parameters' => [
                    'password' => $password,
                ],
            ];

            self::$connection = new Client($parameters, $options);
        }
        return self::$connection;
    }

}