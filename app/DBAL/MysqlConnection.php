<?php

namespace App\DBAL;


use PDO;

class MysqlConnection
{

    private static $connection = null;

    private function __construct()
    {
    }

    public static function getConnection()
    {
        if (self::$connection === null) {
            $host = getConfig('DB_HOST');
            $database = getConfig('DB_DATABASE');
            $user = getConfig('DB_USER');
            $password = getConfig('DB_PASSWORD');
            self::$connection = new PDO("mysql:host=$host;dbname=$database", $user, $password);
        }
        return self::$connection;
    }

}