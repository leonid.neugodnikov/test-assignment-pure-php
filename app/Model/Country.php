<?php

namespace App\Model;


class Country extends Model
{
    protected $table = 'countries';

    public function getAll()
    {
        $sql = "SELECT id, iso from {$this->table}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findOneByIso($iso)
    {
        $sql = "SELECT id, iso from {$this->table} WHERE iso = :iso";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([
            'iso' => $iso,
        ]);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

}