<?php

namespace App\Model;


class Event extends Model
{

    protected $table = 'events';

    public function getAll()
    {
        $sql = "SELECT id, name from {$this->table}";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findOneByName($name)
    {
        $sql = "SELECT id, name from {$this->table} WHERE name = :name";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([
            'name' => $name,
        ]);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

}