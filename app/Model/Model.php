<?php

namespace App\Model;

abstract class Model
{
    protected $connection;

    public function __construct()
    {
        $this->connection = \App\DBAL\MysqlConnection::getConnection();
    }
}