<?php

namespace App\Model;


use DateTime;

class Counter extends Model
{

    protected $table = 'counters';

    public function createOrUpdate($countryId, $eventId, $count)
    {
        $date = (new DateTime())->format('Y-m-d');
        $id = $this->getRowId($date, $countryId, $eventId);
        if ($id) {
            $this->updateCount($id, $count);
        } else {
            $this->create($date, $countryId, $eventId, $count);
        }
    }

    // Getting sum of each event over the last 7 days by country for the top 5 countries of all times
    public function getTopAggregated()
    {
        $sql = "select countries.iso as country, events.name as event, SUM(count) as sum FROM counters 
INNER JOIN countries on countries.id = counters.country_id 
INNER JOIN events on events.id = counters.event_id 
WHERE date >= DATE(NOW()) - INTERVAL 7 DAY 
AND country_id IN (SELECT * FROM (SELECT country_id from counters GROUP BY country_id ORDER BY sum(count) DESC LIMIT 5) temp) 
GROUP BY country_id, event_id";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    protected function getRowId($date, $countryId, $eventId)
    {
        $sql = "SELECT id from {$this->table} where (date = :date AND country_id = :country_id and event_id = :event_id)";
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([
            'date' => $date,
            'country_id' => $countryId,
            'event_id' => $eventId,
        ]);
        return $stmt->fetch(\PDO::FETCH_COLUMN);
    }

    protected function updateCount($id, $count)
    {
        $sql = "UPDATE {$this->table} SET count = count + :count WHERE id = :id";
        $this->connection->prepare($sql)->execute([
            'count' => $count,
            'id' => $id,
        ]);
    }

    public function create($date, $countryId, $eventId, $count)
    {
        $sql = "INSERT INTO {$this->table} (date, country_id, event_id, count) VALUES(:date, :country_id, :event_id, :count)";
        $this->connection->prepare($sql)->execute([
            'date' => $date,
            'country_id' => $countryId,
            'event_id' => $eventId,
            'count' => $count,
        ]);
    }

}