<?php

namespace App\Services;


class CounterFormatter
{
    public static function formatCsv($data)
    {
        $result = "country,event,count\n";
        foreach ($data as $value) {
            $result .= implode(',', $value) . "\n";
        }
        return $result;
    }

    public static function formatJson($data)
    {
        return json_encode($data);
    }
}