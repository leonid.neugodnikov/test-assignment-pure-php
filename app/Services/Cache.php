<?php

namespace App\Services;


use App\DBAL\RedisConnection;
use Closure;

class Cache
{
    public static function remember($key, Closure $callback)
    {
        $connection = RedisConnection::getConnection();

        $value = $connection->get($key);

        if (!is_null($value)) {
            return $value;
        }

        $connection->set($key, $value = $callback());

        return $value;
    }

}